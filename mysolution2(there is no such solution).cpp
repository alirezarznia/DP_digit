//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/
#include<bits/stdc++.h>

#define 	Time 	        	prllf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	prllf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<long long,pii>
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("nice.txt","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-15
#define 	inf 		        1ll<<50
#define     SF                  scanf
#define     PF                  prllf
#define     F                   first
#define     S                   second
#define     vll                 vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;
pii dp[19][2];
ll digitsep(ll x , vector<ll> &vec )
{
    while(x>0)
    {
        vec.push_back(x%10);
        x/=10;
    }
    return 0;
}
pii SUM(ll x ,ll sum , ll ti , vector<ll>&vec)
{
    ll tt =0;
    if(x == -1)
        return {0,1};
   if(!ti && dp[x][ti].F != -1)
        return dp[x][ti];
    ll k = (ti) ? vec[x] : 9;
    ll ret =0;
    Rep(i , k+1)
    {
        ll newti = (vec[x]==i) ? ti : 0;
        pii r =SUM(x-1 , sum+i , newti , vec);
        tt+=r.second;
        ret+=(i*r.second);
        ret+=r.first;
    }
    if(!ti)
        dp[x][ti] = {ret, tt};
    return {ret,tt};

}
vector<ll>veca;
vector<ll>vecb;ll a, b;

ll F()
{
    Set(dp ,-1);
    digitsep(a-1, veca);
    digitsep(b, vecb);
    pii ansa = SUM(veca.size()-1 ,0 ,1 , veca);
    pii ansb = SUM(vecb.size()-1 ,0 ,1 , vecb);
    return ansb.F-ansa.F;
}
int main()
{
   // Test;
    cin>>a>>b;
    cout<<F()<<endl;
}
