#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>
#include 	<fstream>



#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(x, MP(y,z))
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long ,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;
typedef long double ld;
using namespace std;
ll dp[19][250][3];
ll digitsep(ll x , vector<ll> &vec )
{
    while(x>0)
    {
        vec.push_back(x%10);
        x/=10;
    }
    return 0;
}
ll SUM(ll x ,ll sum , ll ti , vector<ll>&vec)
{
    if(x == -1)
        return sum;
    if(!ti && dp[x][sum][ti] != -1)
        return dp[x][sum][ti];
    ll k = (ti) ? vec[x] : 9;
    ll ret =0;
    Rep(i , k+1)
    {
        ll newti = (vec[x]==i) ? ti : 0;
        ret +=SUM(x-1 , sum+i , newti , vec);
    }
    if(!ti)
        dp[x][sum][ti] = ret;
    return ret;

}
vector<ll>veca;
vector<ll>vecb;ll a, b;

ll F()
{
    Set(dp ,-1);
    digitsep(a-1, veca);
    digitsep(b, vecb);
    ll ansa = SUM(veca.size()-1 ,0 ,1 , veca);
    ll ansb = SUM(vecb.size()-1 ,0 ,1 , vecb);
    return ansb-ansa;
}
int main()
{
    cin>>a>>b;
    cout<<F()<<endl;
}
